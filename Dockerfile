from python:3

MAINTAINER harford@gmail.com

COPY Pipfile* etsy-check/ /app/
WORKDIR /app

RUN pip install pipenv && \
    pipenv install --system --deploy --ignore-pipfile

ENV FLASK_APP=etsy.py

CMD ["flask", "run", "--host=0.0.0.0"]
