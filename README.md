# etsy-check

An example Flask app to check Etsy items

## Requirements

* Python 3.6 or higher
* bash
* pip

## Usage

* Clone the repo

```bash
git clone https://gitlab.com/harford/etsy-check
cd etsy-check
```

* Set up your Etsy and Currencylayer API keys in `creds.sh`:

```bash
ETSY_API_KEY=foo
CURRENCY_API_KEY=bar
```

* Install the Python dependencies

```bash
pip install pipenv
pipenv install
```

* Run the app

```bash
pipenv shell
make run
```

This will start the Flask process on http://127.0.0.1:5000/

## API

The app returns JSON information. The page and currency can be specified:

* http://127.0.0.1:5000/?page=10
* http://127.0.0.1:5000/?currency=CAD

### Supported Currencies

* USD (default)
* CAD
* GBP
* EUR

# Docker Image

The app is also available as a [Docker image](https://hub.docker.com/r/alexharford/etsy-check/).

It needs the API keys specified as environment variables when it's run:

```bash
ETSY_API_KEY=foo
CURRENCY_API_KEY=bar

docker run --rm \
    -e ETSY_API_KEY=${ETSY_API_KEY} \
    -e CURRENCY_API_KEY=${CURRENCY_API_KEY} \
    -p 5000:5000 \
    alexharford/etsy-check:latest
```
