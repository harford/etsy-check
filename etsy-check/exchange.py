"""Currency Exchange API interface"""

import time
from logging import Logger
import requests


# {
# "success": true,
# "terms": "https://currencylayer.com/terms",
# "privacy": "https://currencylayer.com/privacy",
# "timestamp": 1527651247,
# "source": "USD",
# "quotes": {
#         "USDUSD": 1,
#         "USDAUD": 1.334302,
#         "USDCAD": 1.30273,
#         "USDPLN": 3.760997,
#         "USDMXN": 19.823695
#         }
# }


class Exchange:
    """Currency Exchange API cache"""

    def __init__(self, api_key: str, logger: Logger):
        self.api_key = api_key
        if logger is not None:
            self.logger = logger.getChild("CurrencyExchange")
        else:
            self.logger = Logger(name="CurrencyExchange")

        self.rates = {"USD": 1.0}
        self.expiry = {}

        self.url = "http://apilayer.net/api/live"

    def _get_db(self) -> dict:
        """Prints the database

        >>> e = Exchange('foo', None)
        >>> e.rates = {'CAD': 1.23}
        >>> e.expiry = {'CAD': 456}
        >>> e._get_db()
        ({'CAD': 1.23}, {'CAD': 456})
        """
        self.logger.debug("rates: {self.rates}")
        self.logger.debug("expiry: {self.expiry}")

        return self.rates, self.expiry

    def _get_payload(self, currencies: list):
        """Returns a payload for the API call


        >>> e = Exchange('foo', None)
        >>> e._get_payload(['CAD', 'MXD'])
        {'access_key': 'foo', 'currencies': 'CAD,MXD', 'format': 1}
        """

        payload = {
            "access_key": self.api_key,
            "currencies": ",".join(currencies),
            "format": 1,
        }

        self.logger.debug("payload: {payload}")

        return payload

    def _check_expired(self, current_time: int, currency: str) -> bool:
        """Checks if the exchange rate for the currency has expired

        >>> e = Exchange('foo', None)
        >>> e.expiry['CAD'] = 123
        >>> e._check_expired(0, 'CAD')
        False
        >>> e._check_expired(456, 'CAD')
        True
        >>> e._check_expired(0, 'FOO')
        True
        """
        if currency in self.expiry:
            if current_time < self.expiry[currency]:
                self.logger.debug("{currency} has expired")
                return False

        self.logger.debug("{currency} has not expired")

        return True

    def _fetch_quotes(self, currencies: list):
        """Get a list of conversions and check them at once

        >>> orig_get = requests.get
        >>> class MockInvalidResult:
        ...     status_code = 400
        >>> def mock_invalid_request(url, params, timeout):
        ...     return MockInvalidResult()
        >>> requests.get = mock_invalid_request
        >>> e = Exchange('foo', None)
        >>> e._fetch_quotes(['CAD'])
        ({}, {})
        >>> class MockResult:
        ...     def __init__(self):
        ...         self.status_code = 200
        ...     def json(self):
        ...         return {'quotes': {}, 'success': True, 'timestamp': {}}
        >>> def mock_request(url, params, timeout):
        ...     return MockResult()
        >>> requests.get = mock_request
        >>> e = Exchange('foo', None)
        >>> e._fetch_quotes(['CAD'])
        ({}, {})
        >>> requests.get = orig_get
        """

        payload = self._get_payload(currencies)
        result = requests.get(self.url, params=payload, timeout=1.0)

        if result.status_code != 200 or not result.json()["success"]:
            self.logger.warn("Failed to fetch quotes")
            return {}, {}

        self.logger.debug("Got quotes for {currencies}")

        return result.json()["quotes"], result.json()["timestamp"]

    def _filter_quotes(self, quotes: dict) -> dict:
        """Quotes come in as 'USDCAD' but since we're always dealing in USD,
        strip that prefix. Don't include USDUSD either.

        >>> e = Exchange('foo', None)
        >>> result = {'USDUSD': 1, 'USDAUD': 1.334302, 'USDCAD': 1.30273, 'USDPLN': 3.760997, 'USDMXN': 19.823695}
        >>> e._filter_quotes(result)
        {'AUD': 1.334302, 'CAD': 1.30273, 'PLN': 3.760997, 'MXN': 19.823695}
        """

        return {k[3:]: v for k, v in quotes.items() if k != "USDUSD"}

    def check_currencies(self, currencies: list, current_time: int):
        """Ensures the currencies are in the cache

        >>> def mock_fetch(currencies: list):
        ...     assert(currencies == ['AUD', 'CAD'])
        ...     return ({'USDAUD': 1.33, 'USDCAD': 1.30}, 1234)
        >>> e = Exchange('foo', None)
        >>> e.rates = {'AUD': 1, 'CAD': 1, 'MXD': 0.5}
        >>> e.expiry = {'AUD': 0, 'CAD': 0, 'MXD': 1000}
        >>> e._fetch_quotes = mock_fetch
        >>> e.check_currencies(['AUD', 'CAD', 'MXD'], 100)
        >>> e.rates['AUD']
        1.33
        >>> e.rates['CAD']
        1.3
        >>> e.rates['MXD']
        0.5
        >>> e.expiry['AUD']
        1234
        >>> e.expiry['CAD']
        1234
        >>> e.expiry['MXD']
        1000
        """

        expired = [
            currency
            for currency in currencies
            if self._check_expired(current_time, currency)
        ]

        self.logger.debug("Expired currencies: {expired}")

        quotes, timestamp = self._fetch_quotes(expired)

        quotes = self._filter_quotes(quotes)

        for currency, rate in quotes.items():
            self.rates[currency] = rate
            self.expiry[currency] = timestamp

    def get_conversion(self, source: str, dest: str) -> float:
        """Gets a conversion rate from a source currency to a dest

        >>> e = Exchange('foo', None)
        >>> e.get_conversion('CAD', 'CAD')
        1.0
        >>> e.rates = {'CAD': 0.5, 'MXD': 0.1}
        >>> e.get_conversion('CAD', 'MXD')
        5.0
        >>> def mock_check(currency, time):
        ...     e.rates[currency[0]] = 1.0
        >>> e.check_currencies = mock_check
        >>> e.get_conversion('CAD', 'AUD')
        0.5
        >>> e.get_conversion('EUR', 'CAD')
        2.0
        """
        if source == dest:
            return 1.0

        # source and dest should have been checked already, just in case it
        # wasn't, check again.
        if source not in self.rates:
            self.check_currencies([source], int(time.time()))

        if dest not in self.rates:
            self.check_currencies([dest], int(time.time()))

        self.logger.debug(
            "Conversion for {source} to {dest}: {self.rates[source]} / {self.rates[dest]}"
        )

        return self.rates[source] / self.rates[dest]
