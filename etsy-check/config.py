import os


class Config(object):
    ETSY_API_KEY = os.environ.get("ETSY_API_KEY")
    # if not ETSY_API_KEY:
    #    raise AttributeError

    CURRENCY_API_KEY = os.environ.get("CURRENCY_API_KEY")
    # if not CURRENCY_API_KEY:
    #    raise AttributeError

    LOG_LEVEL = os.environ.get("ETSY_LOG_LEVEL", "INFO")
