"""Etsy App Routes"""

from app import app, etsy_api
from flask import abort
from flask import request
from flask import jsonify
from flask import url_for


def check_for_valid_currency(currency: str) -> bool:
    """Checks for a supported currency

    >>> check_for_valid_currency('foo')
    False
    >>> check_for_valid_currency('CAD')
    True
    """

    if currency in ["GBP", "CAD", "USD", "EUR"]:
        return True

    app.logger.warn("Invalid currency: {currency}")

    return False


def generate_pagination(page: int, currency: str, f_url_for=url_for) -> dict:
    """Generates pagination data

    >>> def mock_url_for(name, **values):
    ...     page = values['page']
    ...     currency = values['currency']
    ...     return f'/?page={page},currency={currency}'
    >>> generate_pagination(0, 'CAD', f_url_for=mock_url_for)
    {'self': '/?page=0,currency=CAD', 'first': '/?page=0,currency=CAD', 'next': '/?page=1,currency=CAD', 'GBP': '/?page=0,currency=GBP', 'CAD': '/?page=0,currency=CAD', 'USD': '/?page=0,currency=USD', 'EUR': '/?page=0,currency=EUR'}
    >>> generate_pagination(10, 'CAD', f_url_for=mock_url_for)
    {'self': '/?page=10,currency=CAD', 'first': '/?page=0,currency=CAD', 'next': '/?page=11,currency=CAD', 'prev': '/?page=9,currency=CAD', 'GBP': '/?page=10,currency=GBP', 'CAD': '/?page=10,currency=CAD', 'USD': '/?page=10,currency=USD', 'EUR': '/?page=10,currency=EUR'}
    """

    pagination = {
        "self": f_url_for("json", page=page, currency=currency, _external=True),
        "first": f_url_for("json", page=0, currency=currency, _external=True),
        "next": f_url_for("json", page=page + 1, currency=currency, _external=True),
    }

    if page > 0:
        pagination["prev"] = f_url_for(
            "json", page=page - 1, currency=currency, _external=True
        )

    for cur_link in ["GBP", "CAD", "USD", "EUR"]:
        pagination[cur_link] = f_url_for(
            "json", page=page, currency=cur_link, _external=True
        )

    return pagination


@app.route("/", methods=["GET"])
def json():
    """Returns information from the import Etsy API, using a specified
    currency"""

    app.logger.info("In {__name__}")
    app.logger.debug("Request: {request}")

    page = request.args.get("page", 0, type=int)
    currency = request.args.get("currency", "USD", type=str)

    if page < 0:
        abort(400)

    if not check_for_valid_currency(currency):
        abort(400)

    listings = etsy_api.get_listings(page, currency)

    return jsonify(
        {"data": listings, "links": generate_pagination(page, currency, url_for)}
    )
