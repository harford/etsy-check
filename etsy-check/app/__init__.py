import logging

from flask import Flask
from config import Config
from exchange import Exchange
from etsy_api import EtsyApi

app = Flask(__name__)
app.config.from_object(Config)


level = logging.getLevelName(app.config["LOG_LEVEL"])
app.logger.setLevel(level)

exchange = Exchange(api_key=app.config["CURRENCY_API_KEY"], logger=app.logger)
etsy_api = EtsyApi(
    api_key=app.config["ETSY_API_KEY"], exchange=exchange, logger=app.logger
)

from app import routes
