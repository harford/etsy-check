"""Etsy API interface"""

from decimal import Decimal
import time
from logging import Logger
import requests

RESULTS_PER_PAGE = 10


class EtsyApi:
    """Etsy API interface wrapper"""

    def __init__(self, api_key: str, exchange, logger: Logger):
        self.api_key = api_key
        self.exchange = exchange
        if logger is not None:
            self.logger = logger.getChild("EtsyApi")
        else:
            self.logger = Logger(name="EtsyApi")

        self.image_url = "https://openapi.etsy.com/v2/listings/{0}/images"
        self.active_url = "https://openapi.etsy.com/v2/listings/active"

        # Etsy API limits 10 requests per second
        self.recovery_time = 0.5

    def _check_400(self, result):
        """Handles 400 errors. Raises exceptions if it's unrecoverable.

        >>> e = EtsyApi('foo', None, None)
        >>> class MockResult:
        ...     def __init__(self, code):
        ...         self.status_code = code
        >>> m = MockResult(200)
        >>> e._check_400(m)
        >>> m = MockResult(400)
        >>> m.headers = {'X-Error-Detail': 'You have exceeded your quota of 123'}
        >>> e._check_400(m)
        >>> m.headers = {'X-Error-Detail': 'unknown error'}
        >>> try:
        ...     e._check_400(m)
        ... except Exception as exc:
        ...     exc.args
        ('EtsyAPI 400 error',)
        """

        if result.status_code == 400:
            if "X-Error-Detail" in result.headers:
                if result.headers["X-Error-Detail"].startswith(
                    "You have exceeded your quota of"
                ):
                    self.logger.info("Waiting for rate limit recovery")
                    time.sleep(self.recovery_time)
                else:
                    self.logger.info(result.headers)
                    raise Exception("EtsyAPI 400 error")

    def _check_403(self, result):
        """Handles 403 errors. Raises exceptions if it's unrecoverable.

        >>> e = EtsyApi('foo', None, None)
        >>> class MockResult:
        ...     def __init__(self, code):
        ...         self.status_code = code
        >>> m = MockResult(200)
        >>> e._check_403(m)
        >>> m = MockResult(403)
        >>> m.headers = {'X-RateLimit-Remaining': '0'}
        >>> try:
        ...     e._check_403(m)
        ... except Exception as exc:
        ...     exc.args
        ('API daily rate exceeded',)
        >>> m.headers = {}
        >>> try:
        ...     e._check_403(m)
        ... except Exception as exc:
        ...     exc.args
        ('EtsyAPI authentication failed',)
        """
        if result.status_code == 403:
            if "X-RateLimit-Remaining" in result.headers:
                if result.headers["X-RateLimit-Remaining"] == "0":
                    self.logger.info(result.headers)
                    raise Exception("API daily rate exceeded")
            else:
                self.logger.info(result.headers)
                raise Exception("EtsyAPI authentication failed")

    def _make_polite_request(self, url, payload):
        """Makes a request taking into account how the Etsy API handles limits

        >>> orig_get = requests.get # Save the original function
        >>> # Test a successful return
        >>> class MockResult:
        ...    def __init__(self):
        ...        self.status_code = 200
        ...        self.headers = {'status': 'ok'}
        ...
        ...    def json(self):
        ...        return {"success": True, "quotes": [], "timestamp": 0}
        >>> def mock_get(url, params, timeout):
        ...     return MockResult()
        >>> e = EtsyApi('foo', None, None)
        >>> requests.get = mock_get
        >>> r = e._make_polite_request("", {})
        >>> # Test a connection timeout exception
        >>> def mock_exception_get(url, params, timeout):
        ...     raise requests.exceptions.ConnectTimeout()
        >>> requests.get = mock_exception_get
        >>> e = EtsyApi('foo', None, None)
        >>> try:
        ...     r = e._make_polite_request("", {})
        ... except Exception as exc:
        ...     exc.args
        ('EtsyApi failed to get valid response',)
        >>> # Test a 40* error
        >>> class MockInvalidResult:
        ...    def __init__(self):
        ...        self.status_code = 400
        >>> def mock_invalid_get(url, params, timeout):
        ...     return MockInvalidResult()
        >>> def mock_check(result):
        ...     pass
        >>> requests.get = mock_invalid_get
        >>> e = EtsyApi('foo', None, None)
        >>> e._check_400 = mock_check
        >>> e._check_403 = mock_check
        >>> try:
        ...     r = e._make_polite_request("", {})
        ... except Exception as exc:
        ...     exc.args
        ('EtsyApi failed to get valid response',)
        >>> requests.get = orig_get # restore the original function
        """
        retries = 10
        while retries > 0:
            try:
                result = requests.get(url, params=payload, timeout=2.0)

                # If it's a valid result, return it
                if result.status_code == 200:
                    self.logger.debug("Got valid result")
                    return result

                self._check_400(result)
                self._check_403(result)
            # Connection timeouts are safe to retry
            except requests.exceptions.ConnectTimeout:
                self.logger.debug("Connect timeout, retrying")
                time.sleep(0.1)

            retries -= 1

        self.logger.debug("Failed to connect after multiple retries")
        raise Exception("EtsyApi failed to get valid response")

    def _fetch_thumbnail_url(self, listing: int) -> str:
        """Runs the request for image data

        >>> class MockResult:
        ...     def json(self):
        ...        return "[]"
        >>> def mock_request_none(url: str, payload: dict) -> str:
        ...     image_url = "https://openapi.etsy.com/v2/listings/0/images"
        ...     if url == image_url:
        ...         return None
        ...     else:
        ...         return MockResult()
        >>> e = EtsyApi('foo', None, None)
        >>> e._make_polite_request = mock_request_none
        >>> e._fetch_thumbnail_url(0)
        >>> e._fetch_thumbnail_url(1234)
        '[]'
        """

        payload = {"api_key": self.api_key, "fields": "url_75x75"}

        images = self._make_polite_request(self.image_url.format(listing), payload)

        if images is None:
            self.logger.info("No thumbnail found")
            return None

        self.logger.debug("Found thumbnail for {listing}")
        return images.json()

    def get_thumbnail_url(self, listing: int) -> str:
        """Gets the thumbnail image URL for a listing

        >>> def mock_fetch_empty(listing: int) -> str:
        ...     return { 'count': 0 }
        >>> def mock_fetch_valid(listing: int) -> str:
        ...     return { 'count': 1, 'results': [{'url_75x75': 'https://example.com/img.jpg' }]}
        >>> e = EtsyApi('foo', None, None)
        >>> e._fetch_thumbnail_url = mock_fetch_empty
        >>> e.get_thumbnail_url(1234)
        >>> e._fetch_thumbnail_url = mock_fetch_valid
        >>> e.get_thumbnail_url(1234)
        'https://example.com/img.jpg'
        """

        image_data = self._fetch_thumbnail_url(listing)

        if not image_data or image_data["count"] == 0:
            return None

        return image_data["results"][0]["url_75x75"]

    def _fetch_listings(self, page: int) -> list:
        """Gets a list of listings

        >>> class MockResult:
        ...     def json(self):
        ...        return {"results": 1234}
        >>> def mock_request_none(url: str, payload: dict) -> str:
        ...     if payload["offset"] == 0:
        ...         return None
        ...     else:
        ...         return MockResult()
        >>> e = EtsyApi('foo', None, None)
        >>> e._make_polite_request = mock_request_none
        >>> e._fetch_listings(0)
        >>> e._fetch_listings(1234)
        1234
        """

        self.logger.debug("Requesting listings")

        payload = {
            "api_key": self.api_key,
            "fields": "listing_id,price,currency_code,title",
            "limit": RESULTS_PER_PAGE,
            "offset": page * RESULTS_PER_PAGE,
            "sort_on": "score",
        }

        listings = self._make_polite_request(self.active_url, payload)

        if listings is None:
            self.logger.info("Failed to retrieve listings")
            return None

        self.logger.debug("Retrieved listings")

        return listings.json()["results"]

    def _get_thumbnails(self, listings: list):
        """Gets thumbnails for listings


        >>> def mock_get_thumbnail_url(listing_id):
        ...     return f"thumb-{listing_id}"
        >>> e = EtsyApi('foo', None, None)
        >>> e.get_thumbnail_url = mock_get_thumbnail_url
        >>> listings = [{"listing_id": 1234}]
        >>> e._get_thumbnails(listings)
        >>> listings[0]["thumb"]
        'thumb-1234'
        """

        self.logger.debug("Going to get thumbnails")

        for item in listings:
            item["thumb"] = self.get_thumbnail_url(item["listing_id"])

    def _get_required_currencies(self, listings: list, currency: str) -> set:
        """Gets the currencies needed to be checked for the exchange rate

        >>> e = EtsyApi('foo', None, None)
        >>> listings = []
        >>> e._get_required_currencies(listings, 'CAD')
        {'CAD'}
        >>> e._get_required_currencies(listings, 'USD')
        set()
        >>> listings = [{'currency_code': 'AUD'}]
        >>> sorted(list(e._get_required_currencies(listings, 'CAD')))
        ['AUD', 'CAD']
        """

        currencies = {
            item["currency_code"] for item in listings if item["currency_code"] != "USD"
        }

        # Be sure to include the currency we want to convert *to*
        if currency != "USD":
            currencies.add(currency)

        self.logger.debug("Got required currencies")

        return currencies

    def _convert_currency(self, listings: list, currency: str):
        """Converts currencies

        >>> class MockExchange:
        ...    def check_currencies(self, *args):
        ...        pass
        ...    def get_conversion(self, *args):
        ...        return 2.0
        >>> mock_exchange = MockExchange()
        >>> listings = [{"price": 3.0, "currency_code": "foo"}]
        >>> e = EtsyApi('foo', mock_exchange, None)
        >>> def mock_required(listings, currency):
        ...     return ['CAD']
        >>> e._get_required_currencies = mock_required
        >>> e._convert_currency(listings, 'CAD')
        >>> listings[0]
        {'price': Decimal('6.00'), 'currency_code': 'foo'}
        """
        currencies = self._get_required_currencies(listings, currency)

        # Check the cache to be sure things haven't expired
        self.exchange.check_currencies(currencies, int(time.time()))

        for item in listings:
            if "price" in item:

                raw = float(item["price"]) * self.exchange.get_conversion(
                    currency, item["currency_code"]
                )

                item["price"] = round(Decimal(raw), 2)

        self.logger.debug("Converted currencies for {listings}")

    def _cleanup_listings(self, listings: list) -> list:
        """Cleans up some fields that we didn't request, or aren't useful any
        more.

        >>> e = EtsyApi('foo', None, None)
        >>> listings = [{'currency_code': 'AUD'}, {'sku': []}, {'price': 123.45}]
        >>> e._cleanup_listings(listings)
        >>> listings
        [{}, {}, {'price': 123.45}]
        """

        for item in listings:
            if "currency_code" in item:
                del item["currency_code"]
            if "sku" in item:
                del item["sku"]

        self.logger.debug("Cleaned up data for {listings}")

    def get_listings(self, page: int, currency: str) -> dict:
        """Returns the active Etsy listings

        This function will raise exceptions from various API calls and return
        them to the flask framework for logging.

        >>> def mock_fetch_listings(page):
        ...    if page == 0:
        ...        return None
        ...    else:
        ...        return [1]
        >>> def mock_get_thumbnails(listings):
        ...    return None
        >>> def mock_convert_currency(listings, currency):
        ...    return None
        >>> def mock_cleanup_listings(listings):
        ...    return None
        >>> e = EtsyApi('foo', None, None)
        >>> e._fetch_listings = mock_fetch_listings
        >>> e._get_thumbnails = mock_get_thumbnails
        >>> e._convert_currency = mock_convert_currency
        >>> e._cleanup_listings = mock_cleanup_listings
        >>> e.get_listings(0, "CAD")
        >>> e.get_listings(1, "CAD")
        [1]
        """

        listings = self._fetch_listings(page)

        if not listings:
            self.logger.info("No listings found")
            return None

        self._get_thumbnails(listings)
        self._convert_currency(listings, currency)
        self._cleanup_listings(listings)

        self.logger.debug("Got listing data")

        return listings
